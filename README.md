# Capstone Android App Development

This project is for the Coursera Capstone MOOC for "Android App Development", the final part of a series of on-line courses to learn Android development.

An Android application for downloading images will be built in this project.

The images will be downloaded from:

http://lorempixel.com/

The user will be shown the last downloaded picture and be able choose to store favorite pictures locally.

There will be two activities where the first is for downloading the pictures and the second will be used for viewing the favorite pictures.

A service will be used to download the images in the background, and this service will broadcast when current picture download is completed.

A locale database will be present to store the favorite images.

The main components of the application are visualized in the below picture.

![](components.png)
